
if (Drupal.jsEnabled) {
	$(document).ready( function() {
      if ($('body.submitted').is('body') && $('#messages .error').is('div') == false) {
        window.setTimeout(window.close, 2000, true);
      }
      $('a.nodeformpopup').each(function () { 
        var href = this.href;
        $(this).click( function () { Drupal.nodeformpopupLaunch(href); }); 
        $(this).attr({ href: 'javascript:;' });
        });
		});
}

Drupal.nodeformpopupLaunch = function (href) {
  open(href, null, 'modal=1,status=0,scrollbars=1,toolbar=0,resizable=1,width=730,height=500,left='
    + (screen.width-730)/2 +',top='+ (screen.height-500)/2);
}
